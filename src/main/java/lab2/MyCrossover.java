package lab2;

import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyCrossover extends AbstractCrossover<double[]> {
    protected MyCrossover() {
        super(1);
    }

    private static double[] crossover(double[] p1, double[] p2, Random random) {
        double[] res = new double[p1.length];
        for (int dim = 0; dim < p1.length; dim++) {
            if (random.nextDouble() < 0.1) {
                double alpha = 0.1;
                res[dim] = alpha * p1[dim] + (1 - alpha) * p2[dim];
            } else {
                res[dim] = p1[dim];
            }
        }
        return res;
    }

    protected List<double[]> mate(double[] p1, double[] p2, int i, Random random) {
        List<double[]> children = new ArrayList<>();

        // your implementation:
        children.add(crossover(p1, p2, random));
        children.add(crossover(p2, p1, random));
        return children;
    }
}
