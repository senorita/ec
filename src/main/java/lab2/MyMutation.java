package lab2;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyMutation implements EvolutionaryOperator<double[]> {

    private final double t1 = 1;
    private final double t2 = 0.25;

    private static double clip(double val, double clip) {
        return Math.min(Math.max(val, -Math.abs(clip)), Math.abs(clip));
    }

    public List<double[]> apply(List<double[]> population, Random random) {
        // initial population
        // need to change individuals, but not their number!

        // your implementation:

        List<double[]> res = new ArrayList<>();
        for (double[] specimen : population) {
            double[] newSpecimen = new double[specimen.length];
            boolean shouldMutate = random.nextDouble() < 0.01;
            for (int dim = 0; dim < specimen.length; dim++) {
                newSpecimen[dim] = specimen[dim];
                if (shouldMutate) {
                    if (random.nextDouble() > 0.5) {
                        newSpecimen[dim] += random.nextGaussian() * 1;
                    }
                }
            }
            res.add(newSpecimen);
        }
        return res;
    }
}
