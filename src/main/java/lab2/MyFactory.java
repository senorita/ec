package lab2;

import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.Random;

public class MyFactory extends AbstractCandidateFactory<double[]> {

    private int dimension;

    public MyFactory(int dimension) {
        this.dimension = dimension;
    }

    private static double nextDoubleBounds(Random random, double a, double b) {
        if (a > b) {
            throw new RuntimeException("Invalid bounds");
        }
        return random.nextDouble() * (b - a) + a;
    }

    public double[] generateRandomCandidate(Random random) {
        double[] solution = new double[dimension];
        // x from -5.0 to 5.0

        // your implementation:
        for (int i = 0; i < dimension; i++) {
            solution[i] = nextDoubleBounds(random, -5.0, 5.0);
        }

        return solution;
    }
}

